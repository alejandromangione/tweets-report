# Backend Exercise

The purpose of this project is to show my experience and knowledge as a Backend.


## Result

[https://safe-sands-80741.herokuapp.com](https://safe-sands-80741.herokuapp.com).


## Installing and running

1. Clone this repository

`git clone ...`

2. Change to the directory

`cd tweets-report`

3. Install the depencies

`bundle install`

4. Create Database

`rails db:create db:migrate`

5. Run server

`rails s`


## Built With

- Ruby On Rails
- Bundle
- Ruby
- Postgree


## Specs

To run the spec:

`rails test`