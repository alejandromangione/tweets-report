class DashboardController < ApplicationController
  def index
    @last_record = Record.last || Record.new
  end

  def update

    record = Record.create

    record.update_tweets

    if (record.success?)
      redirect_to root_path, notice: 'Tweets have been updated successfully.'
    else
      redirect_to root_path, alert: 'Tweets have not been updated. Something happened! :('
    end
  end

end
