class User < ApplicationRecord
  has_many :tweets, dependent: :destroy

  validates :username, presence: true
end
