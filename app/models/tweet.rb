class Tweet < ApplicationRecord
  belongs_to :user
  belongs_to :record

  scope :with_keywords, -> { where(keyword: true)}
end
