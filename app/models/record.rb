class Record < ApplicationRecord
  has_many :tweets

  enum status: [:success, :failed]

  def update_tweets

    result = TweetService.pull
    count_keywords = 0

    if result.present?
      result.each do |tweet_obj|
        user = User.find_or_create_by(id: tweet_obj.id)
        user.update(username: tweet_obj.user_handle, followers: tweet_obj.followers)

        tweet = Tweet.find_or_create_by(created_at: tweet_obj.created_at)
        tweet.update(user: user, message: tweet_obj.message, sentiment: tweet_obj.sentiment_float, keyword: tweet_obj.keyword, record: self)

        count_keywords += 1 if tweet_obj.keyword
      end

      self.update(total_tweets: result.count, total_keywords: count_keywords)
      self.success!
    else
      self.failed!
    end
  end

  def keyword_percent
    (total_keywords.to_f / total_tweets.to_f)*100
  end

end
