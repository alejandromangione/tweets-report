
class TweetService
  class << self

    API_ENDPOINT = 'https://devtestapiapp.herokuapp.com/'.freeze

    TweetObject = Struct.new(:created_at, :updated_at, :id,:user_handle, :followers, :message, :sentiment) do
      def sentiment_float
        sentiment.to_f
      end

      def keyword
        message.include? 'coke' or message.include? 'coca-cola' or message.include? 'diet-cola'
      end
    end

    def pull
      rtn = []
      response = client.public_send(:get, 'tweets.json')

      if(response.success?)
        begin
          Oj.load(response.body).each do |tweet|
            rtn << TweetObject.new(
              tweet['created_at'],
              tweet['updated_at'],
              tweet['id'],
              tweet['user_handle'],
              tweet['followers'],
              tweet['message'],
              tweet['sentiment']
            )
          end
        rescue => e
          Rails.logger.error e
        end
      end

      rtn
    end

    private

    def client
      @_client ||= Faraday.new(API_ENDPOINT) do |client|
        client.request :url_encoded
        client.adapter Faraday.default_adapter
      end
    end

  end
end