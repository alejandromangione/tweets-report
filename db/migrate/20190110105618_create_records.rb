class CreateRecords < ActiveRecord::Migration[5.2]
  def change
    create_table :records do |t|
      t.integer :total_tweets
      t.integer :total_keywords
      t.integer :status

      t.timestamps
    end
  end
end
