class CreateTweets < ActiveRecord::Migration[5.2]
  def change
    create_table :tweets do |t|
      t.text :message
      t.float :sentiment
      t.boolean :keyword
      t.references :user, foreign_key: true
      t.references :record, foreign_key: true

      t.timestamps
    end
  end
end
