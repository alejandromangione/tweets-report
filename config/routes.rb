Rails.application.routes.draw do

  root 'dashboard#index'
  
  get 'dashboard/update'

  resources :users
  resources :tweets
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
