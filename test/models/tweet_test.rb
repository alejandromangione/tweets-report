require 'test_helper'

class TweetTest < ActiveSupport::TestCase

  setup do
    @tweet = tweets(:one)
    @user = User.create(username: users(:one).username)
    @record = Record.create
  end

  test "should save a new tweet" do
    tweet = Tweet.new(message: @tweet.message, user: @user, record: @record)
    assert tweet.save
  end

end
