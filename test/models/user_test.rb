require 'test_helper'

class UserTest < ActiveSupport::TestCase

  setup do
    @user = users(:one)
  end

  test "should save a new user" do
    user = User.new(username: @user.username)
    assert user.save
  end

  test "should not save user without username" do
    user = User.new(followers: @user.followers)
    assert_not  user.save
  end
end
