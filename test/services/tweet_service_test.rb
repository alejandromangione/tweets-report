require 'test_helper'
require 'vcr'

VCR.configure do |config|
  config.cassette_library_dir = "fixtures/vcr_cassettes"
  config.hook_into :faraday
end


class TweetServiceTest < ActiveSupport::TestCase

  test "should be total of request" do
    VCR.use_cassette("tweets") do
      result = TweetService.pull
      assert_same(result.count, 2)
    end
  end

  test "should have one keyword" do
    VCR.use_cassette("tweets") do
      result = TweetService.pull
      assert result.any? {|t| t.keyword }
    end
  end
end
